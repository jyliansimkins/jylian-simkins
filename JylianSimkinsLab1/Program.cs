﻿using System;

namespace JylianSimkinsLab1
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person1;
            Person person2;
            Person person3;

            person1 = new Person("Jesse", "Bunker");
            person2 = new Person("Jylian", "Simkins");
            person3 = new Person("Patricia", "Coleman");

            Console.WriteLine(person1.ToString());
            Console.WriteLine(person2.ToString());
            Console.WriteLine(person3.ToString());
            Console.ReadLine();

        }
    }
}
