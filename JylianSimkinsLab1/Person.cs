﻿using System;
using System.Collections.Generic;
using System.Text;

namespace JylianSimkinsLab1
{
    class Person
    {
        private String firstName;
        private String lastName;
        public String street { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public string zip { get; set; }

        public Person (string first, string last)
        {
            firstName = first;
            lastName = last;
        }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }

        public string ToString()
        {
            return " First Name: " + FirstName + " Last Name: " + LastName;
        }

        public override bool Equals(object obj)
        {
            return obj is Person equals &&
                   FirstName == equals.FirstName &&
                   LastName == equals.LastName &&
                   street == equals.street &&
                   city == equals.city &&
                   state == equals.state &&
                   zip == equals.zip;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(FirstName, LastName, street, city, state, zip);
        }



    }
}
